# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
export ZSH="{installation_path}/.oh-my-zsh"

ZSH_THEME="agnoster"

plugins=(git)

source $ZSH/oh-my-zsh.sh

export EDITOR=nvim

# Key bindings

bindkey "^W" backward-kill-word
bindkey \^U backward-kill-line

# Setopt options

setopt completeinword
setopt interactivecomments
# Setopt

source ~/.linuxify
source ~/.aliases