#!/usr/bin/env bash

declare -a debian_dependencies=(
    #[START] neovim dependencies
    luajit
    tree-sitter
    neovim
    #[END] neovim dependencies

    #[START] docker dependencies
    ca-certificates
    gnupg
    lsb-release
    #[END] docker depenencies

    adb
    bats
    build-essential
    cmake
    curl
    docker
    gawk
    git
    htop
    jq
    nmap
    openssh-client
    parallel
    python3.6
    rar
    ss
    unrar
    tmux
    wget
    zsh
)
export debian_dependencies

install_common_dependencies() {

    sudo add-apt-repository ppa:deadsnakes/ppa
    sudo apt update -y
    sudo apt upgrade -y

    sudo apt install -y ${debian_dependencies[@]}
}

install_common_dependencies