
Content:

* Bash|Zsh configuration files
* Install common dependencies
* Install ad hoc dependencies:
	- java
	- python 
* Support for multiple distros:
	* Mac
	* Debian-like
* Shortcuts in history file
